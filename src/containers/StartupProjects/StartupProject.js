import React from "react";
import "./StartupProjects.css";
import { bigProjects } from "../../portfolio";
import { Fade } from "react-reveal";
import { VerticalTimeline, VerticalTimelineElement}  from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';
import { MdSchool, MdWork, MdFitnessCenter} from 'react-icons/md';
import { GoLaw } from "react-icons/go";


export default function StartupProject() {

  return (
    
                 <VerticalTimeline>
                  <VerticalTimelineElement
                    className="vertical-timeline-element--work"
                    date="July 2016 - Present"
                    iconStyle={{ background: '#2F65BC', color: '#fff' }}
                    icon={<MdWork/>}
                  >
                      <h4 className="vertical-timeline-element-title">Service Delivery Officer</h4>
                      <h5 className="vertical-timeline-element-subtitle">Kambi Philippines Inc.</h5>
                      <span>Makati City, Philippines</span>
                      <p>
                        Null
                      </p>
                    
                  </VerticalTimelineElement>

                  <VerticalTimelineElement
                    className="vertical-timeline-element--work"
                    date="February 2020 - Present"
                    iconStyle={{ background: '#2F65BC', color: '#fff' }}
                    icon={<MdWork/>}
                  >
                      <h4 className="vertical-timeline-element-title">Quality Controller/Digitizer</h4>
                      <h5 className="vertical-timeline-element-subtitle">Zuitt/Tokyo University</h5>
                      <span>Makati City, Philippines</span>
                      <p>
                        Null
                      </p>
                    
                  </VerticalTimelineElement>





                  <VerticalTimelineElement
                    className="vertical-timeline-element--work"
                    date="2019"
                    iconStyle={{ background: '#2FBC57', color: '#fff' }}
                    icon={<MdSchool/>}
                   
                  >
                      <h4 className="vertical-timeline-element-title">Bootcamper</h4>
                      <h5 className="vertical-timeline-element-subtitle">Zuitt Web Development Bootcamp</h5>
                      <span>Makati City, Philippines</span>  
                      <p>
                        Null
                      </p>
                    </VerticalTimelineElement>

                  <VerticalTimelineElement
                    className="vertical-timeline-element--work"
                    date="June 2016 - August 2016"
                    iconStyle={{ background: '#2F65BC', color: '#fff' }}
                    icon={<MdWork/>}
                  >
                      <h4 className="vertical-timeline-element-title">Second Line Support Officer - Sports Operations</h4>
                      <h5 className="vertical-timeline-element-subtitle">Everlounge Inc.</h5>
                      <span>Makati City, Philippines</span>
                      <p>
                        Null
                      </p>
                     
                  </VerticalTimelineElement>

                  <VerticalTimelineElement
                    className="vertical-timeline-element--work"
                    date="July 2015 - July 2017"
                    iconStyle={{ background: '#2FBC57', color: '#fff' }}
                    icon={<GoLaw/>}
                   
                  >
                      <h4 className="vertical-timeline-element-title">Student</h4>
                      <h5 className="vertical-timeline-element-subtitle">San Beda College of Law</h5>
                      <span>Manila, Philippines</span>  
                      <p>
                        Null
                      </p>
                    </VerticalTimelineElement>
                  <VerticalTimelineElement
                    className="vertical-timeline-element--work"
                    date="May - July 2014 "
                    iconStyle={{ background: '#2FBC57', color: '#fff' }}
                    icon={<MdFitnessCenter/>}
                   
                  >
                      <h4 className="vertical-timeline-element-title">Student</h4>
                      <h5 className="vertical-timeline-element-subtitle">Gold's Fitness Institute</h5>
                      <span>Manila, Philippines</span>  
                      <p>
                        Null
                      </p>
                    </VerticalTimelineElement>

                  <VerticalTimelineElement
                    className="vertical-timeline-element--work"
                    date="June 2016 - August 2016"
                    iconStyle={{ background: '#2F65BC', color: '#fff' }}
                    icon={<MdWork/>}
                  >
                      <h4 className="vertical-timeline-element-title">Assistant Bookmaker</h4>
                      <h5 className="vertical-timeline-element-subtitle">Yew Tree Services Inc.</h5>
                      <span>Makati City, Philippines</span>
                      <p>
                        Null
                      </p>
                     
                  </VerticalTimelineElement>
                  <VerticalTimelineElement
                    className="vertical-timeline-element--work"
                    date="2009-2013"
                    iconStyle={{ background: '#2FBC57', color: '#fff' }}
                    icon={<MdSchool/>}
                   
                  >
                      <h4 className="vertical-timeline-element-title">Student- AB Political Science</h4>
                      <h5 className="vertical-timeline-element-subtitle">Rizal Technological University </h5>
                      <span>Mandaluyong City, Philippines</span>  
                      <p>
                        Null
                      </p>
                    </VerticalTimelineElement>

                    </VerticalTimeline>
  
  );
}