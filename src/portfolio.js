﻿
/* Change this file to get your personal Portfolio */

// Your Summary And Greeting Section

import emoji from "react-easy-emoji";

const greeting = {
  /* Your Summary And Greeting Section */
  username: "Cedric Angelo Eduarte",
  title: "Hi, I'm Ced",

  subTitle: emoji("🚀 Working as a Senior Logistics Associate "),
  subTitle1: emoji("🚀 A Full Stack Web Developer"),
  subTitle2: emoji("🚀 Junior Project Manager/Project Assistant"),
  
  resumeLink: "https://drive.google.com/file/d/1d7tzh-ZYPJNrr-RMvkGZk7Qmtj7XkY-S/view?usp=sharing"  
};

// Your Social Media Link

const socialMediaLinks = {

  gitlab: "https://gitlab.com/eduarte.cedric",
  github: "https://github.com/cededuarte",
  linkedin: "https://www.linkedin.com/in/cedriceduarte/",
  gmail: "eduarte.cedric@gmail.com",
  facebook: "https://www.facebook.com/cededuarte/",
  stackoverflow: "https://stackoverflow.com/users/13984812/ced",
  instagram: "https://www.instagram.com/nuffffced/"
  // Instagram and Twitter are also supported in the links!
};

// Your Skills Section

const skillsSection = {
  title: "What I do",
  subTitle: "SENIOR LOGISTICS ASSOCIATE & FREELANCE FULL WEB STACK DEVELOPER; FOND OF FRONT END DESIGN",
  skills: [

    emoji("🚀 AS A SENIOR  LOGISTICS ASSOCIATE: "),
    emoji("⚡ Handles day-to-day schedule of live traders"),
    emoji("⚡ Handles sports scheduling day-to-day tasks"),

    emoji("🚀 AS A FREELANCE FULL STACK WEB DEVELOPER: "),
    emoji("⚡ Develops highly interactive Front end / User Interfaces for web and mobile applications"),
    emoji("⚡ Progressive Web Applications ( PWA ) in normal and SPA Stacks"),
    emoji("⚡ Automating data collection through python scripts"),
    
  ],


/* Make Sure You include correct Font Awesome Classname to view your icon
https://fontawesome.com/icons?d=gallery */

  softwareSkills: [
    {
      skillName: "html-5",
      fontAwesomeClassname: "fab fa-html5"
    },
    {
      skillName: "css3",
      fontAwesomeClassname: "fab fa-css3-alt"
    },
    {
      skillName: "sass",
      fontAwesomeClassname: "fab fa-sass"
    },
    {
      skillName: "bootstrap",
      fontAwesomeClassname: "fab fa-bootstrap"
    },
    {
      skillName: "mdb",
      fontAwesomeClassname: "fab fa-mdb"
    },
    {
      skillName: "JavaScript",
      fontAwesomeClassname: "fab fa-js"
    },
    {
      skillName: "reactjs",
      fontAwesomeClassname: "fab fa-react"
    },
    {
      skillName: "nodejs",
      fontAwesomeClassname: "fab fa-node"
    },

    {
      skillName: "npm",
      fontAwesomeClassname: "fab fa-npm"
    },
    {
      skillName: "php",
      fontAwesomeClassname: "fab fa-php"
    },
    {
      skillName: "sql-database",
      fontAwesomeClassname: "fas fa-database"
    },
    {
      skillName: "mongoDB",
      fontAwesomeClassname: "fas fa-database"
    },
    {
      skillName: "laravel",
      fontAwesomeClassname: "fab fa-laravel"
    },

    {
      skillName: "python",
      fontAwesomeClassname: "fab fa-python"
    },
    {
      skillName: "git",
      fontAwesomeClassname: "fab fa-git"
    },
    {
      skillName: "jira",
      fontAwesomeClassname: "fab fa-jira"
    }
  ]
};


// Your top 3 proficient stacks/tech experience

const techStack = {
  viewSkillBars: true, //Set it to true to show Proficiency Section
  experience: [
    {
      Stack: "Front End Development",  //Insert stack or technology you have experience in
      progressPercentage: "90%"  //Insert relative proficiency in percentage
    },
    {
      Stack: "Back End Development",
      progressPercentage: "70%"
    },
    {
      Stack: "Project Management",
      progressPercentage: "70%"
    },
    {
      Stack: "Data Management",
      progressPercentage: "60%"
    },
  ]
};


// Your top 3 work experiences

const workExperiences = {
  viewExperiences: true, //Set it to true to show workExperiences Section
  experience: [
    {
      role: "QA/Digitizer",  
      company: "Zuitt/Tokyo University",
      companylogo: require("./assets/images/facebookLogo.png"),
      date: "June 2018 – Present",
      desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
      descBullets: [
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
      ]
    },
    {
      role: "Front-End Developer",   
      company: "Quora",
      companylogo: require("./assets/images/quoraLogo.png"),
      date: "May 2017 – May 2018",
      desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
    },
    {
      role: "Software Engineer Intern",  
      company: "Airbnb",
      companylogo: require("./assets/images/airbnbLogo.png"),
      date: "Jan 2015 – Sep 2015",
      desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
    },
  ]
};




// Some Big Projects You have worked with your company

const bigProjects = {
  title: "Big Projects",
  subtitle: "SOME STARTUPS AND COMPANIES THAT I HELPED TO CREATE THEIR TECH",
  projects: [
    {
      image: require("./assets/images/saayaHealthLogo.webp"),
      link: "http://saayahealth.com/"
    },
    {
      image: require("./assets/images/nextuLogo.webp"),
      link: "http://nextu.se/"
    }
  ]
};

// Your Achievement Section Include Your Certification Talks and More

const achievementSection = {

  title: emoji("Achievements And Certifications 🏆 "),
  subtitle: "Achievements, Certifications, Award Letters and Some Cool Stuff that I have done !",

  achivementsCards: [
    {
      title: "Google Code-In Finalist",
      subtitle: "First Pakistani to be selected as Google Code-in Finalist from 4000 students from 77 different countries.",
      image: require("./assets/images/codeInLogo.webp"),
      footerLink: [
        { name: "Certification", url: "https://drive.google.com/file/d/0B7kazrtMwm5dYkVvNjdNWjNybWJrbndFSHpNY2NFV1p4YmU0/view?usp=sharing" },
        { name: "Award Letter", url: "https://drive.google.com/file/d/0B7kazrtMwm5dekxBTW5hQkg2WXUyR3QzQmR0VERiLXlGRVdF/view?usp=sharing" },
        { name: "Google Code-in Blog", url: "https://opensource.googleblog.com/2019/01/google-code-in-2018-winners.html" }
      ]
    },
    {
      title: "Google Assistant Action",
      subtitle: "Developed a Google Assistant Action JavaScript Guru that is available on 2 Billion devices world wide.",
      image: require("./assets/images/googleAssistantLogo.webp"),
      footerLink: [{ name: "View Google Assistant Action", url: "https://assistant.google.com/services/a/uid/000000100ee688ee?hl=en" }]
    },

    {
      title: "PWA Web App Developer",
      subtitle: "Completed Certifcation from SMIT for PWA Web App Development",
      image: require("./assets/images/pwaLogo.webp"),
      footerLink: [
        { name: "Certification", url: "" },
        { name: "Final Project", url: "https://pakistan-olx-1.firebaseapp.com/" }
      ]
    }
  ]
};

// Blogs Section


// Talks Sections


// Podcast Section


const experienceEducation = {

   expEdu: [
       {
          jobTitle: "AAAA",
          company: "BBBB",
          location: "CCCCCC CCCCCC",
          date: "September 2020",
          description: "trial",
          icon: "MdSchool"
        },  
       {
          jobTitle: "BBB",
          company: "CCC",
          location: "DDDDDD CCCCCC",
          date: "September 2020",
          description: "trial",
          icon: "MdWork"
        },  

       {
          jobTitle: "BBB",
          company: "CCC",
          location: "DDDDDD CCCCCC",
          date: "September 2020",
          description: "trial",
          icon: "MdWork"
        }, 
       {
          jobTitle: "BBB",
          company: "CCC",
          location: "DDDDDD CCCCCC",
          date: "September 2020",
          description: "trial",
          icon: "MdWork"
        }, 
      ]

};
export {greeting, socialMediaLinks, skillsSection, techStack, workExperiences, achievementSection};
